#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main(int argc, char* argv[]) {
	
	mkfifo("/tmp/loggerfifo", 0777);

	int fd=open("/tmp/loggerfifo", O_RDONLY);
	if (fd < 0){
		perror ("Fallo en la apertura de la tuberia");
		return(1);
}
	int aux;
	char buff[200];
	while(1)
	{
		
		aux=read(fd,buff,sizeof(buff));
		printf("%s\n", buff);
		if(aux<0)
		{
			perror("fallo lectura");
			return (1);
		}
		if (aux==0){
			perror("fin programa");
			return (1);
	}
	printf("%s\n",buff);

	}
	close(fd);
	unlink("/tmp/loggerfifo");
	return 0;


}
