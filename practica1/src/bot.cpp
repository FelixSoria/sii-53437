#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main()

{
	int f;
	DatosMemCompartida* pMemComp;
	char* proyeccion;

	f=open("/tmp/datosBot.txt",O_RDWR);
	if(f==-1) {
		printf("Error al abrir el Bot\n");
		//exit(-1);
	}
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,f,0);

	close(f);

	pMemComp=(DatosMemCompartida*)proyeccion;

	while(1)
	{
		usleep(10000);
		float posRaqueta1;
		
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		
		if(posRaqueta1<pMemComp->esfera.centro.y)
			pMemComp->accion1=1;
		else if(posRaqueta1>pMemComp->esfera.centro.y)
			pMemComp->accion1=-1;
		else
			pMemComp->accion1=0;	
		
	}
	munmap(proyeccion,sizeof(*(pMemComp)));
	return 0;

}

